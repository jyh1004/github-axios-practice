# github-axios-practice

## 피드백
### 1) text라는 단어가 검색어(searchText, searchKeyword)보다 추상적인 의미이기에 명확하게 바꿀 것

### 2) axios를 활용하여 api호출하는 부분을 async / await 활용

### 3) uuid를 부여하기보다 repo key로 사용할 값이 있었을듯
- 의미상 uuid가 어떤 값인지 알아채기 어렵다
- list에서 key를 쓴다고 하면... uuid가 매번 바뀌기 때문에, 재로딩했을 때 전부 새로고침하게 됨

### 4) React Fragment는 축약해서 쓸 수 있음 => <> </>

### 5) 최소한의 모듈을 사용하기 위해 필요한 인터페이스를 정의해서 사용한다. 사용자는 axios인지 뭔지 알 필요가 없다.

- userAPI.js

~~~
import axios from "axios";

const instance = axios.create({
    baseURL: 'https://api.github.com/users',
});

const UserAPI = {
    getRepos(user) {
        return instance.get(`${user}/repos`);
    }
};

export default UserAPI;
~~~

- app.js

~~~
import UserAPI from "../utils/api/userAPI"
import React, {useState} from "react";

const App = () => {

    const [searchKeyword, setSearchKeyword] = useState();

    const search = () => {
        UserAPI.getRepos(searchKeyword).then();
    }


}
~~~

### 6) EmptyText에 isEmpty를 굳이 props로 보내 내부에서 렌더링을 결정할 필요가 없다. 렌더링 여부는 외부에서 결정하는 게 훨씬 읽기 쉽다 (절대적인 건 아님)
jsx 한정해서는 &&(단축 평가)를 사용해 조건부 렌더링이 가능하다.

~~~
{flag && (
  <Comp/>
)}
~~~

또는 조건에 따라 A 혹은 B를 렌더링하고 싶다면

~~~
{flag ? (
  <A/>
) : (
  <B/>
)}
~~~

위의 방법을 react에서 적극 활용해주는게 좋다.
