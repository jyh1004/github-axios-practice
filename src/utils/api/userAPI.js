import axios from "axios";

const instance = axios.create({
    baseURL: 'https://api.github.com/users',
    timeout: 10000,
});

const UserAPI = {
    getRepos(username) {
        return instance.get(`/${username}/repos`);
    }
};

export default UserAPI;