import React, { useState } from "react";
import UserAPI from "../utils/api/userAPI";

import SearchKeywordInput from "./SearchKeywordInput";
import SearchButton from "./SearchButton";
import RepositoryList from "./RepositoryList";
import EmptyText from "./EmptyText";

const App = () => {
  const [searchKeyword, setSearchKeyword] = useState("");

  const inputChangeHandler = (event) => setSearchKeyword(event.target.value);

  const [repos, setRepos] = useState([]);

  const searchReposWithUsername = () => {
      UserAPI.getRepos(searchKeyword)
                .then((response) => (response.data.map((elem) => ({
                        id: elem.id,
                        name: elem.name,
                        starCount: elem.stargazers_count
                    }))))
                .then((data) => {
                    setRepos(data);
                    setSearchKeyword('');
                })
                .catch((e) => {
                    setRepos([]);
                    console.log(e);
                });

        // TODO: refactoring using async await 
  };

  const isExistRepos = () => repos.length > 0;

  return (
    <>
      <SearchKeywordInput
        keyword={searchKeyword}
        onChange={inputChangeHandler}
      />
      <SearchButton onClick={searchReposWithUsername}/>
      {isExistRepos() ? 
        <RepositoryList repos={repos}/> : 
        <EmptyText />
      }
    </>
  );
};

export default App;
