import React from "react";
import "./SearchButton.module.less";

const SearchButton = (props) => {
    
    const handleSearchClick = () => (props.onClick());

    return (
        <button type="button" onClick={handleSearchClick}>search</button>
    );
};

export default SearchButton;