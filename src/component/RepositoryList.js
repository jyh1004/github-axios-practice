import React from "react";
import Repository from "./RepositoryItem";

const RepositoryList = (props) => {

    return (
        <ul>
            {
                props.repos.map((repo) => (
                    <Repository
                        key={repo.id}
                        repo={repo}
                    />
                ))
            }
        </ul>
    );
};

export default RepositoryList;