import React from 'react';
import classNames from "classnames/bind";
import styles from "./RepositoryItem.module.less";

const cx = classNames.bind(styles);

const RepositoryItem = (props) => {
    return (
        <li>
            <div>
                <span className={cx('label')}>repository name: </span>
                <span>{props.repo.name}</span>
            </div>
            <div>
                <span className={cx('label')}>repository description: </span>
                <span>{props.repo.description}</span>
            </div>
            <div>
                <span className={cx('label')}>repository star count: </span>
                <span>{props.repo.starCount}</span>
            </div>
        </li>
    );
};

export default RepositoryItem;