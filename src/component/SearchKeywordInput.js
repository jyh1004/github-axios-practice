import React from "react";
import classNames from "classnames/bind";
import styles from "./SearchKeywordInput.module.less";

const cx = classNames.bind(styles);

const SearchKeywordInput = (props) => {

    const handleInputChange = (event) => (props.onChange(event));

    return (
        <input id="search-input" className={cx('search-input')} placeholder="Enter what you want to search for.." 
            value={props.keyword} onChange={handleInputChange} autoFocus />
    );
};

export default SearchKeywordInput;